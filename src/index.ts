import "reflect-metadata";
import {createConnection} from "typeorm";
import App from './app'

//entity 
import {User, Profile, Product, ProductCategory} from './entity/index'

createConnection().then(async connection => {
    const app = new App()
    app.listen()
    // const profile = new Profile();
    // profile.gender = "male";
    // profile.photo = "me.jpg";
    // await connection.manager.save(profile);

    // const user = new User();
    // user.firstName = 'Joe';
    // user.lastName = 'Smith'
    // user.email = 'viscafcb96@gmail.com'
    // user.password= '123456'
    // user.profile = profile;
    // await connection.manager.save(user);

    //test ManyToOne and OneToMany

    const category = new ProductCategory()

    category.name = "Iphone"
    await connection.manager.save(category);

    const product1 = new Product()
    product1.name = "Iphone 5s"
    product1.price = "2000000"
    product1.category = category

    await connection.manager.save(product1);

    const product2 = new Product()
    product2.name = "Iphone XS MAX"
    product2.price = "18000000"
    product2.category = category

    await connection.manager.save(product2);



}).catch(error => console.log(error));
