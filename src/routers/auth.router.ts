
import UserController from '../controllers/user/user.controller'

import {BaseRouter, IBaseRouter} from '../base/base.router'

import middlewares from '../middlewares/index'



const options: IBaseRouter = {
    prefix: "/auth",
    controllers: [{path: '/user', controller: UserController }],
    middlewares: {
        beforeRemote: [],
        afterRemote: []
    }
}
export class AuthRouter extends BaseRouter {
    constructor() {
        super(options)
    }
}
