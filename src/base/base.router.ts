import { NextFunction, Request, Response, Router } from "express";


type TContext = (req: Request, res: Response, next: NextFunction) => void | Promise<void>
interface IController {
    path: string,
    controller: any
}

interface IMiddlewares {
    beforeRemote: TContext[],
    afterRemote: TContext[]
}

export interface IBaseRouter {
    prefix: string,
    controllers: IController[],
    middlewares: IMiddlewares
}


export class BaseRouter {
    public router: Router = Router()
    private options :IBaseRouter

    constructor(options : IBaseRouter) {
        this.options = options
        this.initController()
    }

    initBeforeRemoteMiddleware(router: Router) {
        const {beforeRemote} = this.options.middlewares
        if(beforeRemote && beforeRemote.length) {
            beforeRemote.map(middleware => {
                router.use(middleware)
            })
        }
    }

    initController() {
        const {prefix, controllers} = this.options
        const rt = Router()
        this.initBeforeRemoteMiddleware(rt)

        if(controllers && controllers.length) {
            controllers.map(item => {
                rt.use(item.path, new item.controller().router)
            })
            this.router.use(prefix, rt)
            
        }
    }
}
