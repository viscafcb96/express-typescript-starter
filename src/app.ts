import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as logger from 'morgan'
import * as path from 'path'
import * as session from 'express-session'


import {AuthRouter} from './routers'

export default class App {
    public app: express.Application
    constructor() {
        this.app = express()
        this.setup()
    }

    setup() {
        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'jade');

        this.app.use(logger('dev'));
        this.app.use(cors())
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(cookieParser());
        this.app.use(session({
            secret: 'keyboard cat',
            resave: false,
            saveUninitialized: true,
            cookie: { secure: false}
        }))
        //router
        this.configRouter()
        //static file served
        this.app.use(express.static(path.join(__dirname, 'public')));
        //error handle
        this.initErrorHandle()
    }


    listen() {
        const port = process.env.NODE_PORT || 6001
        this.app.listen(port, () => {
            console.log(`App listening on the port ${port}`);
        });
    }


    configRouter() {
        this.app.use('/', new AuthRouter().router )
    }
    initErrorHandle() {
        this.app.use(function(err, req, res, next) {
            // set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = req.app.get('env') === 'development' ? err : {};
            // render the error page
            res.status(err.status || 500);
            res.render('error');
        });
    }
}