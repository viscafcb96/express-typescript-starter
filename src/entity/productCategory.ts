import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";

import {Product} from './product'

@Entity()
export class ProductCategory {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @OneToMany(type => Product, product => product.category)
    products: Product[]
}