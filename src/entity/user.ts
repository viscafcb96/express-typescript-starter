import {Entity, PrimaryGeneratedColumn, Column,  OneToOne, JoinColumn} from "typeorm";

import {Profile} from "./profile";


@Entity()
export class User {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column({nullable: true})
    age: number;

    @Column({nullable: true})
    phone: number;

    @Column()
    email: string;

    @Column()
    password: string;

    @OneToOne(type => Profile)
    @JoinColumn()
    profile: Profile;
}
