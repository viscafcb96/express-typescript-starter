import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";

import {ProductCategory} from './productCategory'

@Entity()
export class Product {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @Column()
    price: string;
    
    @ManyToOne(type => ProductCategory, productcategory => productcategory.products)
    category: ProductCategory
}
