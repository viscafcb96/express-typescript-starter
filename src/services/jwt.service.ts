import * as jwt from 'jsonwebtoken'
import * as bcrypt from 'bcrypt'

const jwtKey = 'fc_barcelona'

const jwtExpirySeconds = 5*60*60

class JWTService {
    private jwtKey
    private jwtExpiry
    constructor() {
        this.jwtKey = jwtKey
        this.jwtExpiry = jwtExpirySeconds
    }

    verify = (token: string) : Promise<any> => {
        return new Promise((res, rej) => {
            jwt.verify(token, this.jwtKey, {
                algorithm: 'HS256'
            }, (err, decoded) => {
                if(err) rej(err)
                res(decoded)
            }
            )
        })
    }

    sign = (payload: string | Buffer | object): Promise<string> => {
        return new Promise((resolve, reject) => {
            jwt.sign({ payload }, this.jwtKey, {
                algorithm: 'HS256',
                expiresIn: this.jwtExpiry
            }, (err, decoded) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(decoded);
                }
            });
        });
    }

}


export default new JWTService()