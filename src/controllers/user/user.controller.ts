import {Request, Response, NextFunction} from 'express'
import {Router} from 'express'
import {User} from '../../entity/user'
import { getRepository, Like, Not } from 'typeorm';

import jwtService from '../../services/jwt.service'

import * as bcrypt from 'bcrypt'
const saltRounds = 10;



class UserController  {
    public router = Router();
    private userRepository = getRepository(User)

    constructor() {
        this.initializeRoutes()
    }

    private initializeRoutes () {
        this.router.post(`/signup`, this.signup)
        this.router.post(`/login`, this.login)
    }

    signup = async (req: Request, res: Response, next:NextFunction) => {
        const {firstName, lastName, email, password} = req.body

        //check email exist
        let user = await this.userRepository.findOne({
            where: {
                email
            }
        });

        if(user) {
            return res.send({
                code: 30000,
                message: "email already exists"
            })
        }
        // create new record
        user = new User()
        user.firstName = firstName
        user.lastName = lastName
        user.email = email
        try {
            
            user.password = await this.hashPassword(password)
            //save to database
            await this.userRepository.save(user)
            res.send({
                code: 20000,
                message: "Register successfully"
            })
        }catch(e) {
            res.send({
                code: 30000,
                message: e
            })
        }

    }

    login = async (req: Request, res: Response, next:NextFunction) => {
        const {email, password} = req.body
        try {
            let user = await this.userRepository.findOne({where: {email}})
            if(!user) {
                res.send({
                    code: 30000,
                    message: "Tài khoản hoặc mật khẩu không đúng!"
                })
            }else {
                const check = await this.comparePassword(user.password, password)
                if (check) {
                    const payload = {
                        id: user.id,
                        email: user.email
                    }
                    const token = await jwtService.sign(payload)
                    if (token) res.cookie('token', token, { maxAge: 5*60*60*1000 })
                    //delete user password in response
                    delete user.password
                    res.send({
                        code: 20000,
                        data: {...user, token}
                    })
                }else {
                    res.send({
                        code: 30000,
                        message: "Tài khoản hoặc mật khẩu không đúng!"
                    })
                }
            }

        }catch(e) {
            req.send({
                code: 30000,
                message: e
            })
        }
        

    }
    hashPassword = async (password: string) => {
        return await bcrypt.hash(password, saltRounds)
    }
    comparePassword = async (dbPassword : string, uPassword : string) => {
        return await bcrypt.compare(dbPassword, uPassword)
    }

}
export default UserController
