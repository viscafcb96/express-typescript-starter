import { NextFunction, Request, Response } from 'express';
import jwtService from "../services/jwt.service"

class Middleware  {
    authentication = async (req: Request, res: Response, next: NextFunction) => {
        const token = req.cookies.token
        if (!token) return res.status(401).end()
        //verify token
        try {
            await jwtService.verify(token)
            next()
        }catch(e) {
            //bad request
            return res.status(400).end()
        }
        
    }
}

export default new Middleware()